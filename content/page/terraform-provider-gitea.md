---
date: "2020-07-05T12:00:00+02:00"
title: "terraform-provider-gitea"
weight: 60
toc: false
draft: false
menu: "sidebar"
goimport: "code.gitea.io/terraform-provider-gitea git https://gitea.com/gitea/terraform-provider-gitea"
gosource: "code.gitea.io/terraform-provider-gitea https://gitea.com/gitea/terraform-provider-gitea https://gitea.com/gitea/terraform-provider-gitea/src/branch/master{/dir} https://gitea.com/gitea/terraform-provider-gitea/src/branch/master{/dir}/{file}#L{line}"
---

# terraform-provider-gitea - a terraform provider for Gitea


A [terraform](https://www.terraform.io/) [provider](https://gitea.com/gitea/terraform-provider-gitea) for managing a Gitea instance.
