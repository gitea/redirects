---
#date: "2018-12-05T12:00:00+02:00"
title: "log"
slug: "log"
weight: 60
toc: false
draft: false
menu: "sidebar"
goimport: "code.gitea.io/log git https://gitea.com/gitea/log"
gosource: "code.gitea.io/log https://gitea.com/gitea/log https://gitea.com/gitea/log/src/branch/master{/dir} https://gitea.com/gitea/log/src/branch/master{/dir}/{file}#L{line}"
---

# Log - A logging library for Gitea


A [logging library](https://gitea.com/gitea/log) used by Gitea.
