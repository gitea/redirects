---
date: "2020-07-05T12:00:00+02:00"
title: "gitea-vet"
weight: 50
toc: false
draft: false
menu: "sidebar"
goimport: "code.gitea.io/gitea-vet git https://gitea.com/gitea/gitea-vet"
gosource: "code.gitea.io/gitea-vet https://gitea.com/gitea/gitea-vet https://gitea.com/gitea/gitea-vet/src/branch/master{/dir} https://gitea.com/gitea/gitea-vet/src/branch/master{/dir}/{file}#L{line}"
---

# Gitea-Vet - go vet tool for Gitea code


A [go vet tool](https://gitea.com/gitea/gitea-vet) for Gitea code.
