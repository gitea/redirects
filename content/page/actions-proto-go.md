---
date: "2022-11-21T12:00:00+02:00"
title: "actions-proto-go"
slug: "actions-proto-go"
weight: 60
toc: false
draft: false
menu: "sidebar"
goimport: "code.gitea.io/actions-proto-go git https://gitea.com/gitea/actions-proto-go"
gosource: "code.gitea.io/actions-proto-go https://gitea.com/gitea/actions-proto-go https://gitea.com/gitea/actions-proto-go/src/branch/main{/dir} https://gitea.com/gitea/actions-proto-go/src/branch/main{/dir}/{file}#L{line}"
---

# Actions-proto-go - A protobuf library for Gitea actions.


A [protobuf library](https://gitea.com/gitea/actions-proto-go) used by Gitea actions.
