---
date: "2022-03-24T12:00:00+02:00"
title: "distea"
weight: 45
toc: false
draft: false
menu: "sidebar"
goimport: "code.gitea.io/distea git https://gitea.com/gitea/distea"
gosource: "code.gitea.io/distea https://gitea.com/gitea/distea https://gitea.com/gitea/distea/src/branch/main{/dir} https://gitea.com/gitea/distea/src/branch/main{/dir}/{file}#L{line}"
---

# DisTea - Discord bot for Gitea


A Discord bot for the [Gitea server](https://discord.gg/Gitea).
