---
#date: "2018-12-05T12:00:00+02:00"
title: "util"
slug: "util"
weight: 60
toc: false
draft: false
menu: "sidebar"
goimport: "code.gitea.io/pkg/util git https://gitea.com/gitea/util"
gosource: "code.gitea.io/pkg/util https://gitea.com/gitea/util https://gitea.com/gitea/util/src/branch/main{/dir} https://gitea.com/gitea/util/src/branch/main{/dir}/{file}#L{line}"
---

# util - A collection of utilities for Gitea


A [collection of utilities](https://gitea.com/gitea/util) used by Gitea.
